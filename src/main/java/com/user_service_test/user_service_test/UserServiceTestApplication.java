package com.user_service_test.user_service_test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserServiceTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserServiceTestApplication.class, args);
	}

}
